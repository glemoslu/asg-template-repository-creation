# Use the default python 2.7-strech image
FROM python:2.7 

# Install basic linux tools
RUN apt-get update && DEBIAN_FRONTEND=noninteractive \
    apt-get install -y --no-install-recommends \
    apt-utils \
    curl \
    git &&\
    apt-get clean &&\
    apt-get autoclean -y &&\
    apt-get autoremove -y &&\
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install pip from curl
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py &&\
    python get-pip.py

# Install cookiecutter and gitlab needs
RUN apt-get update && DEBIAN_FRONTEND=noninteractive \
    pip install --upgrade cookiecutter python-gitlab &&\
    apt-get clean &&\
    apt-get autoclean -y &&\
    apt-get autoremove -y &&\
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* 

# Clone template repo
RUN git clone "https://github.com/atlas-asg/AnalysisRepositoryTemplate.git" /asg

# Copy cookiecutter variables
COPY ./cookiecutter.json /asg

MAINTAINER Marcelo T. dos Santos <marcelo.teixeira@poli.ufrj.br>
